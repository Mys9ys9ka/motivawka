@include('layouts.header')
<link href="{{ asset('public/css/home/style.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/jquery-ui-1.10.3.min.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/datepicker.min.css') }}" rel="stylesheet">
{{--<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />--}}
<div class="container">
    <div class="add_new_task" title="Добавить новую задачу">+</div>
    <input type='text' id='timepicker-actions-exmpl' />
    <i class="fa fa-futbol-o" aria-hidden="true"></i>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="thermometer_block">
        <div class="term"></div>
        {{--<div class="term"></div>--}}
        {{--<div class="term"></div>--}}
        <div class="term term3"></div>
        {{--<div class="term"></div>--}}
        {{--<div class="term term4"></div>--}}
    </div>
    <br>
    <br>
    {{--<div class="term_block">--}}
    {{--<div class="wrapper">--}}
    {{--<div class="loader">--}}
    {{--<div class="inner"></div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    <br>
    <br>
    <br>

    <div class="alert alert-primary" role="alert">
        A simple primary alert—check it out!
    </div>
    <button type="button" class="btn btn-primary">Primary</button>
    <button type="button" class="btn btn-secondary">Secondary</button>
    <button type="button" class="btn btn-success">Success</button>
    <button type="button" class="btn btn-danger">Danger</button>
    <button type="button" class="btn btn-warning">Warning</button>
    <button type="button" class="btn btn-info">Info</button>
    <button type="button" class="btn btn-light">Light</button>
    <button type="button" class="btn btn-dark">Dark</button>

    <button type="button" class="btn btn-link">Link</button>

    <input class="datepicker-here" data-range="true" data-multiple-dates-separator=" - " data-timepicker="true" data-time-format='hh:ii'>
</div>
@include('layouts.footer')
<div id="newTaskModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"
                    style="float:right;padding:5px 10px 0 0;z-index:1;position:relative;">&times;
            </button>
            <div class="modal-header">
                <span class="modal-title">Новая задача</span>
            </div>
            <div class="modal-body">
                <p>Выбрать рубрику</p>
                <div class="task_category_block">
                    <button type="button" class="btn btn_category btn-info"><i class="fa fa-futbol-o" aria-hidden="true"
                                                                               title="спорт"></i></button>
                    <button type="button" class="btn btn_category btn-info"><i class="fa fa-glass" aria-hidden="true"
                                                                               title="выпивка"></i></button>
                    <button type="button" class="btn btn_category btn-info"><i class="fa fa-language" aria-hidden="true"
                                                                               title="иностранные языки"></i></button>
                    <button type="button" class="btn btn_category btn-info"><i class="fa fa-vk" aria-hidden="true"
                                                                               title="соцсети"></i></button>
                    <button type="button" class="btn btn_category btn-info"><i class="fa fa-wrench" aria-hidden="true"
                                                                               title="ремонт"></i></button>
                    <button type="button" class="btn btn_category btn-info"><i class="fa fa-money" aria-hidden="true"
                                                                               title="заработок"></i></button>
                    <button type="button" class="btn btn_category btn-info"><i class="fa fa-book" aria-hidden="true"
                                                                               title="чтение"></i></button>

                    <input type="hidden" class="task_category">
                </div>
                <p>Выбрать период выполнения</p>
                <div class="task_period_block">
                    <button type="button" class="btn btn_category btn-info">Минуты</button>
                    <button type="button" class="btn btn_category btn-info">Часы</button>
                    <button type="button" class="btn btn_category btn-info">Дни</button>
                    <button type="button" class="btn btn_category btn-info">Недели</button>
                    <button type="button" class="btn btn_category btn-info">Месяца</button>
                    <button type="button" class="btn btn_category btn-info">Годы</button>
                </div>

                {{--<input type='text' class="datepicker-here" data-position="right top" />--}}
                {{--<input type="text" data-range="true" data-multiple-dates-separator=" - " class="datepicker-here"/>--}}
                <input class="datepicker-here" data-range="true" data-multiple-dates-separator=" - ">

                {{--<input id="datepicker" width="276" />--}}
            </div>
            <div class="modal-footer">
                <div class="add_task_to_BD btn btn-success">Добавить</div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        // Инициализация

// Доступ к экземпляру объекта
//         $('.datepicker-here').data('datepicker');
        $.fn.datepicker.language['ru'] =  {
            days: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
            daysShort: ['Вос','Пон','Вто','Сре','Чет','Пят','Суб'],
            daysMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            months: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
            monthsShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
            today: 'Сегодня',
            clear: 'Очистить',
            dateFormat: 'dd.mm.yyyy',
            timeFormat: 'hh:ii',
            firstDay: 1
        };

        // Зададим стартовую дату
        var start = new Date(),
            prevDay,
            startHours = 9;

        // 09:00
        start.setHours(9);
        start.setMinutes(0);

        // Если сегодня суббота или воскресенье - 10:00
        if ([6,0].indexOf(start.getDay()) != -1) {
            start.setHours(10);
            startHours = 10
        }

        $('#timepicker-actions-exmpl').datepicker({
            timepicker: true,
            startDate: start,
            autoClose: true,
            minHours: startHours,
            maxHours: 18,
            onSelect: function(fd, d, picker) {
                // Ничего не делаем если выделение было снято
                if (!d) return;

                var day = d.getDay();

                // Обновляем состояние календаря только если была изменена дата
                if (prevDay != undefined && prevDay == day) return;
                prevDay = day;

                // Если выбранный день суббота или воскресенье, то устанавливаем
                // часы для выходных, в противном случае восстанавливаем начальные значения
                if (day == 6 || day == 0) {
                    picker.update({
                        minHours: 10,
                        maxHours: 16
                    })
                } else {
                    picker.update({
                        minHours: 9,
                        maxHours: 18
                    })
                }
            }
        })
    });
</script>


{{--<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>--}}
<script src = "{{ asset('public/js/jquery-ui.min.js') }}" ></script>
<script src="{{ asset('public/js/home/script.min.js') }}"></script>
<script src="{{ asset('public/js/daterangepicker.js') }}"></script>
<script src="{{ asset('public/js/datapicker.min.js') }}"></script>